
import { useContext } from "react";
import UserContext from '../UserContext';
import { Navigate } from "react-router-dom";

export default function Settings(){

    const { user } = useContext(UserContext);

    return(
        (user.id !== null && user.email !== null)?
        <div>
            <h1>Settings Page</h1>
        </div>
        :
        <Navigate to="/" />
    )
}