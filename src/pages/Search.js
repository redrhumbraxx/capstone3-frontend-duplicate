
import React from "react";
import { Link } from "react-router-dom";
import { Container } from "react-bootstrap";
import error from "../images/error.png"

export default function Search(){
    
    return (
        <Container fluid className="text-light">
            <div className="p-2">
                <h3>Page Not Found</h3>
                <p>Go back to the <Link to="/">homepage</Link>.</p>
            </div>
            <img src={error} className="not-found col-8 offset-2" img-fluid/>
        </Container>
    )
}