import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isActive, setIsActive] = useState(false);

    function registerUser(event){
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(async data =>{

            if(data){
                Swal.fire({
                    title: "Email Already Exists",
                    icon: "error",
                    text: "Kindly use a different email address."
                })
            }
            else{
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        fullName: fullName,
                        email: email,
                        password: password,
                        mobileNo: mobileNo
                    })
                })
                .then(async res => res.json())
                .then(async data => {

                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Thank you for registering!"
                        });
                        setFullName('');
                        setEmail('');
                        setPassword('');
                        setMobileNo('');
                        navigate("/login");
                    }
                    else{
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        });
                    }
                })
            }
        })
    }

    useEffect(() => {
         if(fullName !== '' && email !== '' && password !== '' && mobileNo !== ''){
             setIsActive(true);
         }
         else{
             setIsActive(false);
         }
    }, [fullName, email, password, mobileNo])

    return(
        (user.id !== null && user.email !== null)?
        <Navigate to ="/login" />
        :
        <div className="page-size d-flex align-items-center text-light">
            <Container id="page-opacity" className='register-form container-fluid col-lg-3 col-sm-10 col-md-8'>
                <Form onSubmit={(event) => registerUser(event)}>
                    <h2 className='pt-3 text-center'>Register</h2>
                    <Form.Group controlId="fullName" className='pt-2'>
                        <Form.Label>Full Name</Form.Label>
                        <Form.Control 
                            type="fullName" 
                            placeholder="Enter full name" 
                            value = {fullName}
                            onChange = {event => setFullName(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="userEmail" className='pt-2'>
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email address" 
                            value = {email}
                            onChange = {event => setEmail(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password" className='pt-2'>
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Enter password" 
                            value = {password}
                            onChange = {event => setPassword(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="mobileNo" className='pt-2'>
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control 
                            type="mobileNo" 
                            placeholder="Enter mobile number"
                            value = {mobileNo}
                            onChange = {event => setMobileNo(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <div className='pt-3 pb-3 text-center'>
                        { isActive ?
                            <Button variant="outline-light" type="submit" id="submit-btn-login-register">
                                Register
                            </Button>
                            :
                            <Button variant="outline-light" type="submit" id="submit-btn-login-register" disabled>
                                Register
                            </Button>
                        }
                    </div>
                </Form>
            </Container>
        </div>
    )
}
