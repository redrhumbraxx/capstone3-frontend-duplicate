
import { useContext, useState, useEffect } from "react";
import UserContext from '../UserContext';
import { Navigate, Link } from "react-router-dom";
import { Table, Container, Button, Card } from "react-bootstrap";
import Swal from "sweetalert2";


export default function AdminOrderedProducts(){
    const { user } = useContext(UserContext);
    const [allOrders, setAllOrders] = useState([]);

    const completed = (id) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/admin/${id}/userOrders`, {
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Order Completed",
					icon: "success",
					text: "All items were paid and delivered"
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Error Completion",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/admin/allUsers`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

		setAllOrders(data.map(user => {
            return (
                <div key={user._id} id="card-margin" className="col-lg-5 col-md-5">
                    {(user.totalAmount == 0)?
                        <span></span>
                    :
                        <Card className='text-white bg-white p-3'>
                        <Card.Body>
                            <Card.Title>Customer Name: {user.fullName}</Card.Title>
                            <Card.Subtitle>Email: {user.email}</Card.Subtitle>
                            <br/>
                            <Card.Subtitle>Ordered Menu:</Card.Subtitle>
                            <Card.Text>{user.orders.map(product => {
                                return(
                                    <div key={product._id}>
                                        <div className="d-inline-block col-11">
                                        {product.productName}
                                        </div>
                                        <div className="d-inline-block col-1">
                                        {product.quantity}
                                        </div>
                                    </div>
                                )
                            })}</Card.Text>
                            <Card.Subtitle className="pt-2">Total Purchased: ₱ {user.totalAmount}.00</Card.Subtitle><br/>
                            <div className='text-end'>
                                <Button size="sm" onClick={() => completed(user._id)}>Complete Order</Button>
                            </div>
                        </Card.Body>
                        </Card>
                    }
                </div>
            )
			}));
		});
	}
    useEffect(()=>{
		fetchData();
	}, [])



    return(
        (user.isAdmin === 'true')
        ?
        <div className="text-white pt-5">
            <Container>
                <div className="mb-3 text-center text-light">
                    <h1>Admin Dashboard(Ordered Products)</h1>
                    <Button variant="success" className="mx-2" as={Link} to="/admin">Back to main dashboard</Button>
                </div>

                <div className="row">
                    {allOrders}
                </div>
            </Container>
        </div>
        :
        <Navigate to="/products" />
    )
}
