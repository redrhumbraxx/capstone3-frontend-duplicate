
import { useContext, useState, useEffect } from "react";
import UserContext from '../UserContext';
import { Navigate, Link } from "react-router-dom";
import { Table, Container, Button } from "react-bootstrap";
import Swal from "sweetalert2";


export default function AdminDeleteData(){
    const { user } = useContext(UserContext);
    const [allUsers, setAllUsers] = useState([]);
    const [allProducts, setAllProducts] = useState([]);


    // This is for User Database Deletion
    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/admin/allUsers`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
		setAllUsers(data.map(user => {
				return (
					<tr key={user._id} className="d-flex">
						<td className="text-light col-4">{user.fullName}</td>
						<td className="text-light col-4">{user.email}</td>
						<td className="text-light col-4 text-center">
                            <Button variant="danger" size="sm" onClick={() => deleteAccount(user._id, user.fullName)}>Delete Account</Button>
						</td>
					</tr>
				)
			}));
		});
	}
    useEffect(()=>{
		fetchData();
	}, [])

    const deleteAccount = (userId, userName) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/admin/${userId}/deleteUser`, {
            method: "DELETE",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Account Deleted",
                    icon: "success",
                    text: `${userName} has been deleted from the database.`
                });
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Delete Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                });
            }
        })
    }
    // End of User Database Deletion


    // This is for Product Database Deletion
    const fetchProductData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/allMenu`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
		setAllProducts(data.map(menu => {
				return (
					<tr key={menu._id} className="d-flex">
						<td className="text-light col-4">{menu._id}</td>
						<td className="text-light col-4">{menu.name}</td>
						<td className="text-light col-4 text-center">
                            <Button variant="danger" size="sm" onClick={() => deleteMenu(menu._id, menu.name)}>Delete Product</Button>
						</td>
					</tr>
				)
			}));
		});
	}
    useEffect(()=>{
		fetchProductData();
	}, [])

    const deleteMenu = (menuId, menuName) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/admin/${menuId}/deleteMenu`, {
            method: "DELETE",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Menu Deleted",
                    icon: "success",
                    text: `${menuName} has been deleted from the database.`
                });
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Delete Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                });
            }
        })
    }
    // End of Product Database Deletion


    return(
        (user.isAdmin === 'true' && user.email === 'jordanjeffgo@mail.com')
        ?
        <Container>
            <div className="pt-5 mb-3 text-center text-light">
                <h1>Reset Database</h1>
                <Button variant="success" className="mx-2" as={Link} to="/admin">Back to main dashboard</Button>
            </div>

            <div className="pt-5 mb-3 text-center text-light">
                <h2>Reset Menu Database</h2>
            </div>
            <Table striped bordered hover className="bg-dark">
                <thead>
                    <tr className="text-center d-flex">
                        <th className="col-4">Product Unique ID</th>
                        <th className="col-4">Product Name</th>
                        <th className="col-4">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {allProducts}
                </tbody>
            </Table>
            <br/>

            <div className="pt-5 mb-3 text-center text-light">
                <h2>Reset User Database</h2>
            </div>
            <Table striped bordered hover className="bg-dark">               
                <thead>
                    <tr className="text-center d-flex">
                        <th className="col-4">Full Name</th>
                        <th className="col-4">User Email</th>
                        <th className="col-4">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {allUsers}
                </tbody>
            </Table>
        </Container>
        :
        <Navigate to="/products" />
    )
}