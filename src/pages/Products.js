
import ProductCard from '../components/ProductCard';
import { useEffect, useState, useContext } from "react";
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Container, Form, Button } from 'react-bootstrap';
import pizzaLogo from '../images/buttons/pizza.jpg';
import pastaLogo from '../images/buttons/pasta.jpg';
import sidesLogo from '../images/buttons/sides.jpg';
import beveragesLogo from '../images/buttons/beverages.jpg';

export default function Products(){

    const { user } = useContext(UserContext);
    const [products, setProducts] = useState([]);
    const [data, setData] = useState([]);
    const [filterValue, setfilterValue] = useState('');

    useEffect(() => {
        const fetchData = () => {fetch(`${process.env.REACT_APP_API_URL}/products/menus`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setData(data);
            setProducts(data);
            })
        }
    
    fetchData();
    }, [])
    
    const handleFilter = (event) => {
        if(event.target.value === ''){
            setData(products);
        }
        else{
            const filterResult = products.filter(item => item.name.toLowerCase().includes(event.target.value.toLowerCase()) || item.type.toLowerCase().includes(event.target.value.toLowerCase()))
            setData(filterResult);
        }
        setfilterValue(event.target.value)
    }

    return (
        <div>
            <Container>
                <h1 className="text-center pt-3">Menu</h1>
                <div className='offset-lg-3 col-lg-6 mb-4'>
                    <Button id="logo-btn" className='col-3' as={Link} to='/pizza'>
                        <img src={pizzaLogo} className="logo-btn" />
                    </Button>
                    <Button id="logo-btn" className='col-3' as={Link} to='/pasta'>
                        <img src={pastaLogo} className="logo-btn" />
                    </Button>
                    <Button id="logo-btn" className='col-3' as={Link} to='/sides'>
                        <img src={sidesLogo} className="logo-btn" />
                    </Button>
                    <Button id="logo-btn" className='col-3' as={Link} to='/beverages'>
                        <img src={beveragesLogo} className="logo-btn" />
                    </Button>
                </div>
                <div className="col-lg-4 offset-lg-4">
                    <Form.Control size="lg" className="mb-4" placeholder='Search' value={filterValue} onInput={(e) => handleFilter(e)} />
                </div>
                <div className='row'>
                    {
                        data.map(product => {
                            return(
                                <ProductCard key={product.id} productProp={product} />
                            )
                        })
                    }
                </div>
            </Container>
        </div>
    )
}