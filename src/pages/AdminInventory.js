
import { useContext, useState, useEffect } from "react";
import UserContext from '../UserContext';
import { Navigate, Link } from "react-router-dom";
import { Table, Container, Button, Card } from "react-bootstrap";
import Swal from "sweetalert2";


export default function AdminOrderedProducts(){
    const { user } = useContext(UserContext);
    const [allOrders, setAllOrders] = useState([]);

    const completed = (id) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/admin/${id}/deleteOrders`, {
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Successful Reset",
					icon: "success",
					text: "Inventory of this menu is now back to zero"
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Reset Failed",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/menus`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

		setAllOrders(data.map(menu => {
				return (
                    <div id="card-margin" className="col-lg-5 col-md-5">
                        <Card key={menu._id} className='text-dark bg-muted p-3'>
                        <Card.Body>
                            <Card.Title>Product Name: {menu.name}</Card.Title>
                            <Card.Subtitle>Price: {menu.price}</Card.Subtitle>
                            <br/>
                            <Card.Subtitle>Total Quantity: {menu.orders.length}</Card.Subtitle>
                            <h4 className="pt-2">Total Sales: ₱ {menu.price * menu.orders.length}.00</h4>
                            <div className='text-end'>
                                <Button size="sm" onClick={() => completed(menu._id)}>Reset Inventory</Button>
                            </div>
                        </Card.Body>
                        </Card>
                    </div>
				)
			}));
		});
	}
    useEffect(()=>{
		fetchData();
	}, [])

    return(
        (user.isAdmin === 'true')
        ?
        <div className="text-white pt-5">
            <Container>
                <div className="mb-3 text-center text-light">
                    <h1>Admin Dashboard(Inventory)</h1>
                    <Button variant="success" className="mx-2" as={Link} to="/admin">Back to main dashboard</Button>
                </div>

                <div className="row">
                    {allOrders}
                </div>
            </Container>
        </div>
        :
        <Navigate to="/products" />
    )
}