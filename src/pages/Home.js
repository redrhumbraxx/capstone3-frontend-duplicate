import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, Fragment } from "react";
import { Container, Card } from "react-bootstrap";
import banner from "../images/banner.png";


export default function Home(){

    const { user } = useContext(UserContext);

    return(
        <Fragment>
            <Container>
                <img src={banner} className="banner img-fluid" />
            </Container>

            <div className="home-page-size container bg-white">
                <h2 className="text-center pt-5 pb-3">Mission and Vision</h2>
                <div className="row text-center p-3">
                    <div className="d-inline-block col-lg-6 col-md-6 ps-3 pe-3">
                        <h3>Mission</h3>
                        <p className="paragraph-mv">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum justo id nulla efficitur dictum. Nulla ex augue, cursus lacinia dapibus sed, imperdiet non ex. Pellentesque ac elementum ante. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce mauris erat, scelerisque vitae posuere eu, tincidunt a nibh. Aliquam pharetra orci eget mollis sagittis. Donec auctor enim ut sem rutrum, a aliquam eros congue. Aenean lacinia maximus eros quis posuere. Proin lobortis, turpis non tempus interdum, risus libero tristique ipsum, a consectetur sem leo id est. Integer sagittis purus at condimentum sodales. Mauris blandit orci eget rhoncus gravida. Quisque odio turpis, facilisis sit amet ultricies nec, placerat et magna. Morbi tincidunt, ex eu egestas aliquam, lectus nulla aliquet tortor, nec porta enim massa quis felis. Quisque volutpat eu justo a eleifend. Integer fringilla accumsan est, eu mollis arcu placerat eget.</p>
                    </div>
                    <div className="d-inline-block col-lg-6 col-md-6 ps-3 pe-3">
                        <h3>Vision</h3>
                        <p className="paragraph-mv">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum justo id nulla efficitur dictum. Nulla ex augue, cursus lacinia dapibus sed, imperdiet non ex. Pellentesque ac elementum ante. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce mauris erat, scelerisque vitae posuere eu, tincidunt a nibh. Aliquam pharetra orci eget mollis sagittis. Donec auctor enim ut sem rutrum, a aliquam eros congue. Aenean lacinia maximus eros quis posuere. Proin lobortis, turpis non tempus interdum, risus libero tristique ipsum, a consectetur sem leo id est. Integer sagittis purus at condimentum sodales. Mauris blandit orci eget rhoncus gravida. Quisque odio turpis, facilisis sit amet ultricies nec, placerat et magna. Morbi tincidunt, ex eu egestas aliquam, lectus nulla aliquet tortor, nec porta enim massa quis felis. Quisque volutpat eu justo a eleifend. Integer fringilla accumsan est, eu mollis arcu placerat eget.</p>
                    </div>
                </div>
            </div>

            <div className="footer-color text-center">
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3  pt-5 pb-3">
                    <h3>About Us</h3>
                    <p>Mission & Vision</p>
                    <p>Founders</p>
                    <p>Branches</p>
                </div>
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3 pb-3">
                    <h3>Careers</h3>
                    <p>Pizza Chef</p>
                    <p>Manager</p>
                    <p>Accountant</p>
                </div>
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3 pb-3">
                    <h3>Follow Us</h3>
                    <p>Facebook</p>
                    <p>LinkedIn</p>
                    <p>Instagram</p>
                </div>
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3 pb-4">
                    <h3>Contact Us</h3>
                    <p>(+63)916-911-1111</p>
                    <p>(+63)918-922-2222</p>
                    <p>empizza@mail.com.ph</p>
                </div>
            </div>
        </Fragment>
    )
}